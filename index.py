import keras
from keras.layers import Dense, LSTM, Flatten, GlobalAveragePooling2D, TimeDistributed, Conv2D, Dropout
from keras.models import Model, Sequential
from keras.applications.vgg16 import VGG16


# create a VGG16 model
def CNN_LSTM():
    vgg = VGG16(include_top=False,
                weights='imagenet',
                input_shape=(224, 224, 3)
                )
    for layers in vgg.layers[:-4]:
        layers.trainable = False

    model = Sequential()
    model.add(
        TimeDistributed(
            vgg, input_shape=(5, 224, 224, 3)
        ))
    model.add(TimeDistributed(
        GlobalAveragePooling2D()
    ))

    model.add(LSTM(256, activation='relu', return_sequences=False))

    # finalize
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4, activation='softmax'))
    model.compile('adam', loss='categorical_crossentropy')

    print(model.summary())
    return model
